package com.awesomeapps.flickrsearchapp.model;

import java.io.Serializable;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public class Response implements Serializable {
    private Photos photos;
    private String stat;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    @Override
    public String toString() {
        return "Response{" +
                "photos=" + photos +
                ", stat='" + stat + '\'' +
                '}';
    }
}
