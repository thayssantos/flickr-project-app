package com.awesomeapps.flickrsearchapp.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public class Photos implements Serializable {
    private int page;
    private int pages;
    private int perPage;
    private int total;
    private List<Photo> photo;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }
}
