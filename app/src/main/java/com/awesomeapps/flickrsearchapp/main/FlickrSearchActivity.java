package com.awesomeapps.flickrsearchapp.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.awesomeapps.flickrsearchapp.R;
import com.awesomeapps.flickrsearchapp.view.imagesearch.ImageSearchFragment;

public class FlickrSearchActivity extends AppCompatActivity {
    private ImageSearchFragment mImageSearchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flickrsearch);

        if (savedInstanceState == null) {

            mImageSearchFragment = new ImageSearchFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, mImageSearchFragment)
                    .commit();
        }
    }
}
