package com.awesomeapps.flickrsearchapp.network;

import com.awesomeapps.flickrsearchapp.BuildConfig;
import com.awesomeapps.flickrsearchapp.util.AppConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public class FlickrRetrofitHelper {
    public static final String API_KEY = AppConfig.FLICKR_API_KEY;

    public static FlickrService getFlickrService() {
        final Retrofit retrofit = createRetrofit();
        return retrofit.create(FlickrService.class);
    }

    public static Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.FLICKR_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    public static OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}
