package com.awesomeapps.flickrsearchapp.network;

import com.awesomeapps.flickrsearchapp.model.Response;
import com.awesomeapps.flickrsearchapp.util.FlickrConfig;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public interface FlickrService {
    @GET(FlickrConfig.ENDPOINT)
    Observable<Response> getPhotos(
            @Query(FlickrConfig.QUERY_PARAM_METHOD) String method,
            @Query(FlickrConfig.QUERY_PARAM_API_KEY) String apiKey,
            @Query(FlickrConfig.QUERY_PARAM_TEXT) String text,
            @Query(FlickrConfig.QUERY_PARAM_FORMAT) String format,
            @Query(FlickrConfig.QUERY_PARAM_PER_PAGE) int perPage,
            @Query(FlickrConfig.QUERY_PARAM_PAGE) int page,
            @Query(FlickrConfig.QUERY_PARAM_NOJSONCALLBACK) int noJsonCallback);
}
