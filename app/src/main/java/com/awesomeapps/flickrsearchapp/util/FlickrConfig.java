package com.awesomeapps.flickrsearchapp.util;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public final class FlickrConfig {
    public static final String ENDPOINT = "services/rest/";

    public static final String GET_PHOTOS_METHOD = "flickr.photos.search";

    public static final String QUERY_PARAM_METHOD = "method";
    public static final String QUERY_PARAM_API_KEY = "api_key";
    public static final String QUERY_PARAM_TEXT = "text";
    public static final String QUERY_PARAM_PER_PAGE = "per_page";
    public static final String QUERY_PARAM_PAGE = "page";
    public static final String QUERY_PARAM_FORMAT = "format";
    public static final String QUERY_PARAM_NOJSONCALLBACK = "nojsoncallback";
}