package com.awesomeapps.flickrsearchapp.presenter.imagesearch;

import com.awesomeapps.flickrsearchapp.model.Response;
import com.awesomeapps.flickrsearchapp.network.FlickrRetrofitHelper;
import com.awesomeapps.flickrsearchapp.network.FlickrService;
import com.awesomeapps.flickrsearchapp.util.AppConfig;
import com.awesomeapps.flickrsearchapp.util.FlickrConfig;
import com.awesomeapps.flickrsearchapp.view.imagesearch.ImageSearchFragment;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public class ImageSearchPresenter {
    private ImageSearchFragment mImageSearchFragment;
    private FlickrService mFlickrService;
    private CompositeDisposable mCompositeDisposable;
    private final String JSON_QUERY_VALUE = "json";
    private final int JSON_CALLBACK_QUERY_VALUE = 1;
    private final int QUERY_PER_PAGE = 10;

    public ImageSearchPresenter(ImageSearchFragment imageSearchFragment) {
        mImageSearchFragment = imageSearchFragment;
        mFlickrService = FlickrRetrofitHelper.getFlickrService();
        mCompositeDisposable = new CompositeDisposable();
    }

    private Observable<Response> createObservable(String text, int page) {
        return mFlickrService.getPhotos(FlickrConfig.GET_PHOTOS_METHOD, AppConfig.FLICKR_API_KEY, text,
                JSON_QUERY_VALUE, QUERY_PER_PAGE, page, JSON_CALLBACK_QUERY_VALUE);
    }

    public void getPhotos(String text, int page) {
        Observable<Response> photosObservable = createObservable(text, page);

        mCompositeDisposable.add(photosObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(createObserver()));
    }

    private DisposableObserver<Response> createObserver() {
        return new DisposableObserver<Response>() {
            @Override
            public void onNext(Response response) {
                if ((response != null) && (response.getPhotos() != null)
                        && (response.getPhotos().getPhoto() != null)
                        && (response.getPhotos().getTotal() > 0)) {
                    mImageSearchFragment.addPhotos(response.getPhotos().getPhoto());
                } else {
                    mImageSearchFragment.showMessageNotFound();
                }
                mImageSearchFragment.stopProgressBar();
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        };
    }
}