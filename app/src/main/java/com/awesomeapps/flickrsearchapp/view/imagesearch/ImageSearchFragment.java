package com.awesomeapps.flickrsearchapp.view.imagesearch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.awesomeapps.flickrsearchapp.R;
import com.awesomeapps.flickrsearchapp.model.Photo;
import com.awesomeapps.flickrsearchapp.presenter.imagesearch.ImageSearchPresenter;

import java.util.List;

public class ImageSearchFragment extends Fragment {
    private ImageSearchPresenter imageSearchPresenter;
    private EditText mSearchText;
    private Button mSearchButton;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdapter;
    private RelativeLayout mProgressBar;
    private RecyclerViewScrollListener mScrollListener;

    private int nextPage;
    private String text;
    private View view;

    public ImageSearchFragment() {
        imageSearchPresenter = new ImageSearchPresenter(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_image_search, container, false);

        mSearchButton = (Button) view.findViewById(R.id.search_button);
        mSearchText = (EditText) view.findViewById(R.id.search_text);
        mProgressBar = (RelativeLayout) view.findViewById(R.id.rl_progress_bar);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text = mSearchText.getText().toString();

                if(text != null && !text.equalsIgnoreCase("")) {
                    nextPage = 1;
                    imageSearchPresenter.getPhotos(text, nextPage);
                    mProgressBar.setVisibility(View.VISIBLE);
                    clearInfo();
                } else {
                    Toast.makeText(getContext(), R.string.empty_search, Toast.LENGTH_LONG).show();
                }
                hideKeyboard();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_search_result);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerAdapter = new RecyclerAdapter(getContext());
        mRecyclerView.setAdapter(mRecyclerAdapter);

        mScrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                
                getMorePhotos();
                final int curSize = mRecyclerAdapter.getItemCount();

                view.post(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerAdapter.notifyItemRangeInserted(curSize,
                                mRecyclerAdapter.getItemCount() - 1);
                    }
                });
            }
        };
        mRecyclerView.addOnScrollListener(mScrollListener);
        return view;
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void clearInfo() {
        mSearchText.setText("");
        mRecyclerAdapter.clearList();
        mScrollListener.resetState();
    }

    private void getMorePhotos() {
        nextPage++;
        imageSearchPresenter.getPhotos(text, nextPage);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void addPhotos(List<Photo> photos) {
        mRecyclerAdapter.add(photos);
        mProgressBar.setVisibility(View.GONE);
    }

    public void stopProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    public void showMessageNotFound() {
        Toast.makeText(getContext(), R.string.photos_not_found, Toast.LENGTH_LONG).show();
    }
}