package com.awesomeapps.flickrsearchapp.view.imagesearch;

import com.awesomeapps.flickrsearchapp.R;
import com.awesomeapps.flickrsearchapp.model.Photo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableResource;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thays Santos Duarte on 27/10/2017.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context mContext;
    private List<Photo> mPhotos;
    private final String SCHEME = "https";
    private final String AUTHORITY_FRST_PART = "farm";
    private final String AUTHORITY_SND_PART = ".staticflickr.com";
    private final String FINAL_PATH = ".jpg";

    public RecyclerAdapter(Context context) {
        mContext = context;
        mPhotos = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photo photo = mPhotos.get(position);
        Glide.with(mContext).load(getImageUrl(photo))
                .thumbnail(0.5f)
                .placeholder(R.mipmap.ic_placeholder)
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivPhoto);

        holder.tvTitle.setText(photo.getTitle());
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    public void add(List<Photo> photos) {
        mPhotos.addAll(photos);
        notifyDataSetChanged();
    }

    public String getImageUrl(Photo photo) {
        Uri.Builder mImageUrl = new Uri.Builder();
        mImageUrl.scheme(SCHEME)
                .authority(AUTHORITY_FRST_PART + photo.getFarm() + AUTHORITY_SND_PART)
                .appendPath(photo.getServer())
                .appendPath(photo.getId()+ "_" + photo.getSecret() + FINAL_PATH);

        return mImageUrl.toString();
    }

    public void clearList() {
        mPhotos.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final ImageView ivPhoto;
        public final TextView tvTitle;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ivPhoto = (ImageView) view.findViewById(R.id.iv_photo);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
        }
    }
}
