# README #

Android Project to display a list of photos from Flickr API

### Architecture ###

* MVP simplified

### Android Studio Version ###

* 2.3.3

### Minimum and Target SDK Version ###

* Min: 22
* Target: 25

### External Dependencies ###

* RxJava2
* RxAndroid
* Retrofit2
* Gson
* Glide
